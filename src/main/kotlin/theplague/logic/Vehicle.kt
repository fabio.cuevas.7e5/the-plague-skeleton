package theplague.logic

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import kotlin.math.abs

abstract class Vehicle(timesLeft: Int): Item(timesLeft), Iconizable {
    open fun canMove(from: Position, to: Position): Boolean = true

}

class OnFoot(timesLeft: Int): Vehicle(timesLeft) {
    override val icon: String
        get() = "\uD83D\uDEB6"

    override fun canMove(from: Position, to: Position): Boolean {
        for (i in -1..1) {
            if (to == Position(from.x,from.y+i) || to == Position(from.x+i,from.y) ) {
                return true
            }
        }
        return false
    }
}

class Bicycle(timesLeft: Int): Vehicle(timesLeft) {
    override val icon: String
        get() = "\uD83D\uDEB2"

    override fun canMove(from: Position, to: Position): Boolean {
        for (i in -2..2) {
            if (to == Position(from.x,from.y+i) || to == Position(from.x+i,from.y) ) {
                return true
            }
        }
        return false
    }
}

class Helicopter(timesLeft: Int): Vehicle(timesLeft) {
    override val icon: String
        get() = "\uD83D\uDE81"
}