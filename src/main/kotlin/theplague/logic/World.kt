package theplague.logic

import theplague.interfaces.*


data class World(override val width: Int = 5,
            override val height: Int = 3,
            override val player: Player = Player(Position(2, 1)),
            override val territories: List<List<Territory>> = List(height) {List(width) {Territory()}}): IWorld {

    init { territories[1][2].player = player }

    val playerTerritory get() = territories[player.currentPosition.y][player.currentPosition.x]

    /**
     * generate the new colonies and items, reproduces the colonies and expands them
     */
    override fun nextTurn() {
        generateNewItems()
        generateNewColonies()
        reproduce()
        player.turns += 1
    }

    /**
     * Returns true if the game has finished
     */
    override fun gameFinished(): Boolean {return false}



    /**
     * Returns true if the player can move to the given position using the current vehicle
     */
    override fun canMoveTo(position: Position) : Boolean {
        return player.canMoveTo(position)
    }

    /**
     * Moves the player to the new position
     */
    override fun moveTo(position: Position) {
        playerTerritory.player = null
        player.currentPosition = position
        territories[position.y][position.x].player = player
        player.useVehicle()
    }

    /**
     * The player exterminates the plague
     */
    override fun exterminate() {
        playerTerritory.exterminate(player.currentWeapon)
    }

    /**
     * Returns the item than the player can take if any
     */
    override fun takeableItem(): Iconizable? {
        return playerTerritory.item
    }

    /**
     * The player takes the item
     */
    override fun takeItem() {
        val item = playerTerritory.item
        player.equip(item!!)
        playerTerritory.item = null

    }


    fun randomPosition(): Position = TODO()

    fun randomTerritory(): Territory = territories[(0 until height).random()][(0 until width).random()]

    fun generateNewColonies() {
        val randomTerritory = randomTerritory()
        when ((1..100).random()) {
            in 1..30 -> randomTerritory.colony = Ant()
            in 31..40 -> randomTerritory.colony = Dragon(0.5)
            in 41..100 -> randomTerritory.iconList() == randomTerritory.iconList()
        }
    }

    fun place(colonization: Colonization) {}

    fun generateNewItems() {
        val randomTerritory = randomTerritory()
        when ((1..100).random()) {
            in 1..25 -> randomTerritory.item = Bicycle(5)
            in 26..35 -> randomTerritory.item = Helicopter(5)
            in 36..60 -> randomTerritory.item = Broom(0)
            in 61..70 -> randomTerritory.item = Sword(0)
            in 71..100 -> randomTerritory.iconList() == randomTerritory.iconList()
        }
    }

    fun reproduce() {
        for (y in 0..territories.lastIndex) {
            for (x in 0..territories[y].lastIndex) {
                territories[y][x].colony?.reproduce()
            }
        }
    }

    fun expand() {}



}
