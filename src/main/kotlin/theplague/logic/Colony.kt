package theplague.logic

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import kotlin.random.Random.Default.nextDouble


abstract class Colony(): Iconizable{

    abstract var size: Int

    abstract fun willReproduce(): Boolean // porcientos de reproduccion segun colonia

    fun reproduce() { // cada turno, aumentar o no tamaño segun willreproduce
        if (willReproduce()) {
            size += 1
        }
    }

    abstract fun needsToExpand() // cuando es mayor que 3 - retorna boolean

    abstract fun attacked(weapon: Weapon) // disminuir size segun arma

    abstract fun colonizedBy(plague: Colony): Colony // Si la colonia es mas fuerte que la actual devuelve plage sino return this

    abstract fun expand(position: Position, maxPosition: Position): List<Colonization> // segun needtoExpand pasa a otra casilla de al lado
}

data class Ant(val reproductionTax: Double = 0.3): Colony() {

    override val icon: String
        get() = "\uD83D\uDC1C"

    override var size: Int = 0

    override fun willReproduce(): Boolean {
        return if (size <= 2) {
            when (nextDouble(1.0)) {
                in 0.0..reproductionTax -> true
                else -> false
            }
        } else {
            false
        }
    }

    override fun needsToExpand() {
        TODO("Not yet implemented")
    }


    override fun attacked(weapon: Weapon) {
        when (weapon) {
            is Broom -> size = 0
            is Sword -> size -= 1
            is Hand -> size -= 2
        }
    }

    override fun colonizedBy(plague: Colony): Colony {
        TODO("Not yet implemented")
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        TODO("Not yet implemented")
    }
}

data class Dragon(var constTimeToReproduce: Double): Colony() {

    override val icon: String
        get() = "\uD83D\uDC09"

    override var size: Int = 0


    override fun willReproduce(): Boolean {
        return if (size <= 2) {
            constTimeToReproduce -= 0.1
            constTimeToReproduce == 0.0
        } else {
            false
        }
    }


    override fun needsToExpand() {
        TODO("Not yet implemented")
    }


    override fun attacked(weapon: Weapon) {
        when (weapon) {
            is Broom -> size == size
            is Sword -> size -= 1
            is Hand -> size == size
        }
    }

    override fun colonizedBy(plague: Colony): Colony {
        TODO("Not yet implemented")
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        TODO("Not yet implemented")
    }
}